import React, { useState } from 'react';

const Input = () => {
    const [name, setName] = useState('');

    return (
        <>
            <div className="vasya" style={{border: '1px solid gray'}}>
                <label htmlFor="name">Enter your name: </label>
                <input
                    type="text"
                    id="name"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                />
            </div>
            <p>Some text here</p>
        </>
    );
};